from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def dhammanussati():
	dlg = QDialog()
	dlg.setWindowTitle("DHAMMĀNUSSATI")
	dlg.setFixedSize(500, 280)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti:</p>
<p><b>Handa mayaṁ Dhammānussati-nayaṁ karoma se. </b></p>
<p><i>Marilah kita mengucapkan Perenungan terhadap Dhamma.</i></p>

<p>Bersama-sama: </p>
<p><b>Svākkhāto Bhagavatā Dhammo, <br>
Sandiṭṭhiko akāliko ehipassiko, <br>
Opanayiko paccattaṁ veditabbo viññūhī'ti.</b></p>

<p>Dhamma Sang Bhagavā telah sempurna dibabarkan; <br>
Berada sangat dekat, tak lapuk oleh waktu, mengundang untuk dibuktikan; <br>
Menuntun ke dalam batin, dapat diselami oleh para bijaksana dalam batin <br>
masing-masing.</p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
