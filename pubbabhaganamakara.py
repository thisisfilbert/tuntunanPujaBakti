from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def pubbabhaganamakara():
	dlg = QDialog()
	dlg.setWindowTitle("PUBBABHĀGANAMAKĀRA")
	dlg.setFixedSize(410, 200)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti: </p>
<p><b>Handa mayaṁ Buddhassa Bhagavato Pubba-bhāga-namakāraṁ <br>
karoma se. </b></p>	
<p>Bersama-sama: </p>
<p><b>Namo Tassa Bhagavato Arahato Sammā-Sambuddhassa</b></p>

<p>Terpujilah Sang Bhagavā, Yang Maha Suci, <br>
Yang Telah Mencapai Penerangan Sempurna</p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
