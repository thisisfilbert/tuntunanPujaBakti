from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def tisarana():
	dlg = QDialog()
	dlg.setWindowTitle("TISARAṆA")
	dlg.setFixedSize(500, 500)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti:</p>
<p><b>Handa mayaṁ Ti-saraṇa-gamana-pāṭhaṁ bhaṇāma se. </b></p>
<p><i>Marilah kita mengucapkan Tiga Perlindungan.</i></p>

<p>Bersama-sama: </p>
<p><b>Buddhaṁ saraṇaṁ gacchāmi. <br>
Dhammaṁ saraṇaṁ gacchāmi. <br>
Saṅghaṁ saraṇaṁ gacchāmi. </b><p>

<p><b>Dutiyampi Buddhaṁ saraṇaṁ gacchāmi. <br>
Dutiyampi Dhammaṁ saraṇaṁ gacchāmi. <br>
Dutiyampi Saṅghaṁ saraṇaṁ gacchāmi. </b></p>

<p><b> Tatiyampi Buddhaṁ saraṇaṁ gacchāmi. <br>
Tatiyampi Dhammaṁ saraṇaṁ gacchāmi. <br>
Tatiyampi Saṅghaṁ saraṇaṁ gacchāmi. </b></p>

<p>Aku berlindung kepada Buddha. <br>
Aku berlindung kepada Dhamma. <br>
Aku berlindung kepada Saṅgha. </p>

<p>Untuk kedua kalinya, aku berlindung kepada Buddha. <br>
Untuk kedua kalinya, aku berlindung kepada Dhamma. <br>
Untuk kedua kalinya, aku berlindung kepada Saṅgha. </p>

<p>Untuk ketiga kalinya, aku berlindung kepada Buddha. <br>
Untuk ketiga kalinya, aku berlindung kepada Dhamma. <br>
Untuk ketiga kalinya, aku berlindung kepada Saṅgha. </p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
