from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def pancasila():
	dlg = QDialog()
	dlg.setWindowTitle("PAÑCASĪLA")
	dlg.setFixedSize(500, 350)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti:</p>
<p><b>Handa mayaṁ Pañca-sikkhā-pada-pāṭhaṁ bhaṇāma se. </b></p>
<p><i>Marilah kita mengucapkan Lima Latihan Sīla.</i></p>

<p>Bersama-sama: </p>
<p><b>Pāṇātipātā veramaṇī sikkhā-padaṁ samādiyāmi.<br>
Adinnādānā veramaṇī sikkhā-padaṁ samādiyāmi. <br>
Kāmesu micchācārā veramaṇī sikkhā-padaṁ samādiyāmi. <br>
Musāvādā veramaṇī sikkhā-padaṁ samādiyāmi. <br>
Surā-meraya-majja-pamādaṭṭhānā veramaṇī sikkhā-padaṁ samādiyāmi. </b></p>

<p>Aku bertekad akan melatih diri menghindari pembunuhan makhluk hidup.<br>
Aku bertekad akan melatih diri menghindari pengambilan barang yang tidak <br> diberikan. <br>
Aku bertekad akan melatih diri menghindari perbuatan asusila. <br>
Aku bertekad akan melatih diri menghindari ucapan yang tidak benar. <br>
Aku bertekad akan melatih diri menghindari segala minuman keras yang <br>
dapat menyebabkan lemahnya kesadaran. </p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
