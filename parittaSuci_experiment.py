from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from namakara_gatha import namakara_gatha
from pubbabhaganamakara import pubbabhaganamakara
from tisarana import tisarana
from pancasila import pancasila
from buddhanussati import buddhanussati
from dhammanussati import dhammanussati
from sanghanussati import sanghanussati
from saccakiriya_gatha import saccakiriya_gatha
import sys


class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()

		self.setWindowTitle("Tuntunan Puja Bakti")
		self.setFixedSize(250, 350)
		
		#Start of Menu Bar
		menu_bar = self.menuBar()
		more_menu = menu_bar.addMenu('&More')
		
		def menu_clicked():
			dlg = QDialog(self)
			dlg.setWindowTitle("About")
			dlg.setFixedSize(300, 100)
			
			label = QLabel()
			label.setText(
			'''
			<p><b>Tuntunan Puja Bakti</b></p>
			<p> Copyright © Filbert Salim 2024 </p>
			<p> Source of Pali Chants: <a href="http://sasanasubhasita.org/berita-44-paritta-tuntutan-puja-bakti.html" style="color: inherit;">Vihara Sasana Subhasita</a></p>
			<p> License: MIT </p> 
			''')
			label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
			layout = QVBoxLayout()
			layout.addWidget(label)
			dlg.setLayout(layout)
			
			dlg.exec()	
			
		more_menu.addAction('About', menu_clicked)
		more_menu.addAction('Exit', self.destroy)
		#End of Menu Bar
					
		button1 = QPushButton("NAMAKĀRA GĀTHĀ")
		button1.clicked.connect(namakara_gatha)
		button2 = QPushButton("PUBBABHĀGANAMAKĀRA")
		button2.clicked.connect(pubbabhaganamakara)
		button3 = QPushButton("TISARAṆA")
		button3.clicked.connect(tisarana)
		button4 = QPushButton("PAÑCASĪLA")
		button4.clicked.connect(pancasila)
		button5 = QPushButton("BUDDHĀNUSSATI")
		button5.clicked.connect(buddhanussati)
		button6 = QPushButton("DHAMMĀNUSSATI")
		button6.clicked.connect(dhammanussati)
		button7 = QPushButton("SAṄGHĀNUSSATI")
		button7.clicked.connect(sanghanussati)
		button8 = QPushButton("SACCAKIRIYĀ GĀTHĀ")
		button8.clicked.connect(saccakiriya_gatha)

		layout = QVBoxLayout()
		layout.addWidget(button1)
		layout.addWidget(button2)
		layout.addWidget(button3)
		layout.addWidget(button4)
		layout.addWidget(button5)
		layout.addWidget(button6)
		layout.addWidget(button7)
		layout.addWidget(button8)
		
		container = QWidget()
		container.setLayout(layout)
		# Set the central widget of the Window.
		self.setCentralWidget(container)


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
