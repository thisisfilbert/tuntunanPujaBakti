from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def saccakiriya_gatha():
	dlg = QDialog()
	dlg.setWindowTitle("SACCAKIRIYĀ GĀTHĀ")
	dlg.setFixedSize(500, 600)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti:</p>
<p><b>Handa mayaṁ Sacca-kiriyā gāthāyo bhaṇāma se.</b></p>
<p><i>Marilah kita mengucapkan Pernyataan Kebenaran.</i></p>

<p>Bersama-sama: </p>
<p><b>Natthi me saraṇaṁ aññaṁ <br>
Buddho me saraṇaṁ varaṁ <br>
Etena sacca-vajjena <br>
Sotthi me/te hotu sabbadā. </b></p>

<p><b>Natthi me saraṇaṁ aññaṁ <br>
Dhammo me saraṇaṁ varaṁ <br>
Etena sacca-vajjena <br>
Sotthi me/te hotu sabbadā. </b></p>

<p><b>Natthi me saraṇaṁ aññaṁ <br>
Saṅgho me saraṇaṁ varaṁ <br>
Etena sacca-vajjena <br>
Sotthi me/te hotu sabbadā.</b></p>

<p>Tiada perlindungan lain bagiku <br>
Sang Buddha-lah sesungguhnya Pelindungku <br>
Berkat kesungguhan pernyataan ini <br>
Semoga aku/anda selamat sejahtera.</p>

<p>Tiada perlindungan lain bagiku <br>
Dhamma-lah sesungguhnya Pelindungku <br>
Berkat kesungguhan pernyataan ini <br>
Semoga aku/anda selamat sejahtera. </p>

<p>Tiada perlindungan lain bagiku <br>
Saṅgha-lah sesungguhnya Pelindungku <br>
Berkat kesungguhan pernyataan ini <br>
Semoga aku/anda selamat sejahtera.</p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
