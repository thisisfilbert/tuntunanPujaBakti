# Tuntunan Puja Bakti
## About
This Pali Chants are primarily intended for Indonesians. Currently consists of NAMAKĀRA GĀTHĀ, PUBBABHĀGANAMAKĀRA, TISARAṆA, PAÑCASĪLA, BUDDHĀNUSSATI, DHAMMĀNUSSATI, SAṄGHĀNUSSATI and SACCAKIRIYĀ GĀTHĀ.
Everyone is free to fork this project and develop based on own interests.

## Contact
Please kindly look for me at [Sharkey](https://sakurajima.social/@filburutto), a fork of Misskey. 
