from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def sanghanussati():
	dlg = QDialog()
	dlg.setWindowTitle("SAṄGHĀNUSSATI")
	dlg.setFixedSize(500, 550)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti:</p>
<p><b>Handa mayaṁ Saṅghānussati-nayaṁ karoma se. </b></p>
<p><i>Marilah kita mengucapkan Perenungan terhadap Saṅgha.</i></p>

<p>Bersama-sama: </p>
<p><b>Supaṭipanno Bhagavato sāvaka-saṅgho, <br>
Uju-paṭipanno Bhagavato sāvaka-saṅgho, <br>
Ñāya-paṭipanno Bhagavato sāvaka-saṅgho, <br>
Sāmīci-paṭipanno Bhagavato sāvaka-saṅgho, <br> </b></p>

<p><b>Yadidaṁ cattāri purisa-yugāni aṭṭha purisa-puggalā: <br>
Esa Bhagavato sāvaka-saṅgho, <br>
Āhuneyyo pāhuneyyo dakkhiṇeyyo añjali-karaṇīyo, <br>
Anuttaraṁ puññakkhettaṁ lokassā'ti.</b></p>

<p>Saṅgha Siswa Sang Bhagavā telah bertindak baik;<br>
Saṅgha Siswa Sang Bhagavā telah bertindak lurus;<br>
Saṅgha Siswa Sang Bhagavā telah bertindak benar;<br>
Saṅgha Siswa Sang Bhagavā telah bertindak patut;</p>

<p>Mereka, merupakan empat pasang makhluk, terdiri dari <i>delapan jenis <br>
Makhluk Suci*</i>: Itulah Saṅgha Siswa Sang Bhagavā; <br>
Patut menerima pemberian, tempat bernaung, persembahan serta <br>
penghormatan; Lapangan untuk menanam jasa, yang tiada taranya di alam <br>
semesta.</p>

<p><i>*) Mereka disebut Ariya Saṅgha: makhluk-makhluk yang telah <br>
mencapai Sotāpatti Magga dan Phala, Sakadāgāmī Magga dan Phala, <br>
Anāgāmī Magga dan Phala, dan Arahatta Magga dan Phala. </i></p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
