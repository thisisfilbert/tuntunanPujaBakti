from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def namakara_gatha():
	dlg = QDialog()
	dlg.setWindowTitle("NAMAKĀRA GĀTHĀ")
	dlg.setFixedSize(500, 300)
			
	label = QLabel()
	label.setText(
	'''
<p><b>Arahaṁ Sammā-Sambuddho Bhagavā, <br>
Buddhaṁ Bhagavantaṁ abhivādemi.</b></p>

<p>Sang Bhagavā, Yang Maha Suci, Yang Telah Mencapai Penerangan Sempurna;<br>
aku bersujud di hadapan Sang Buddha, Sang Bhagavā. </p>

<p><b>Svākkhāto Bhagavatā Dhammo, <br>
Dhammaṁ namassāmi. </b></p>

<p>Dhamma telah sempurna dibabarkan oleh Sang Bhagavā; <br>
aku bersujud di hadapan Dhamma. </p>

<p><b>Supaṭipanno Bhagavato sāvaka-saṅgho, <br>
Saṅghaṁ namāmi. </b></p>

<p>Saṅgha Siswa Sang Bhagavā telah bertindak sempurna; <br>
aku bersujud di hadapan Saṅgha. </p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
