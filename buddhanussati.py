from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout

def buddhanussati():
	dlg = QDialog()
	dlg.setWindowTitle("BUDDHĀNUSSATI")
	dlg.setFixedSize(500, 350)
			
	label = QLabel()
	label.setText(
	'''
<p>Pemimpin Puja Bakti:</p>
<p><b>Handa mayaṁ Buddhānussati-nayaṁ karoma se. </b></p>
<p><i>Marilah kita mengucapkan Perenungan terhadap Buddha.</i></p>

<p>Bersama-sama: </p>
<p><b>Iti pi so Bhagavā Arahaṁ Sammā-Sambuddho, <br>
Vijjā-caraṇa-sampanno Sugato Lokavidū, <br>
Anuttaro purisa-damma-sārathi satthā deva-manussānaṁ Buddho <br>
Bhagavā'ti. </b></p>

<p>Demikianlah Sang Bhagavā, Yang Maha Suci, Yang Telah Mencapai <br>
Penerangan Sempurna; <br>
Sempurna pengetahuan serta tindak-tanduk-Nya, Sempurna menempuh Sang <br>
Jalan (ke Nibbāna), Pengenal segenap alam; <br>
Pembimbing manusia yang tiada taranya, Guru para dewa dan manusia, <br>
Yang Sadar (Bangun), Yang patut Dimuliakan. </p>
	''')
			
	layout = QVBoxLayout()
	layout.addWidget(label)
	dlg.setLayout(layout)
			
	dlg.exec()
